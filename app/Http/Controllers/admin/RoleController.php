<?php

namespace App\Http\Controllers\admin;

use App\Models\Role;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Response;


class RoleController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role:Admin');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('admin.role');
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => ['required', 'string', 'max:100', 'unique:roles', 'regex:/^\S*$/u'],
        ]);


        if($validator->passes()) {
            // Store your data in database
            $data = new Role();
            $data->name = ucfirst($request->name);
            $data->description = !empty($request->description) ? $request->description : '';
            $data->save();
            $request->session()->flash('flash_success');
            return Response::json(['success' => '1']);
        }

        return Response::json(['errors' => $validator->errors()]);

    }

    public function show(Request $request)
    {
        $totalData = Role::count();
        $totalFiltered = $totalData;

        if(empty($request->input('search.value')))
        {
            $roles = Role::all();
        }
        else {
            $search = $request->input('search.value');
            $roles =  Role::where('name','LIKE',"%{$search}%")->get();
            $totalFiltered = Role::where('name','LIKE',"%{$search}%")->count();
        }

        $data = array();
        if(!empty($roles))
        {
            foreach ($roles as $key=>$role)
            {

                $nestedData['id'] = $key+1;
                $nestedData['name'] = $role->name;
                $nestedData['description'] = $role->description;
                $nestedData['actions'] = "<a href='javascript:void(0)' id='dataEdit' class='btn btn-xs btn-default' data-toggle='modal' data-target='#myModal' data-id='{ \"id\":$role->id,\"name\":\"$role->name\",\"description\":\"$role->description\" }'><i class='fa fa-edit'></i> Edit</a>
                                          &emsp;<a href='javascript:void(0)' onclick='delete_data($role->id,\"$role->name\")' class='btn btn-xs btn-default'><i class='fa fa-trash'></i> Delete</a>";
                $data[] = $nestedData;

            }
        }

        $json_data = array(
            "draw"            => intval($request->input('draw')),
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data"            => $data
        );

        echo json_encode($json_data);

    }

    public function destroy(Request $request, $id)
    {
        Role::find($id)->delete($id);
        $request->session()->flash('flash_danger');
        return Response::json(['success' => '1']);
    }

    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name' => ['required', 'string', 'max:100', 'regex:/^\S*$/u'],
        ]);

        if($validator->passes()) {
            $data = Role::find($id);
            $data->name = ucfirst($request->name);
            $data->description = !empty($request->description) ? $request->description : '';
            if ($data->isDirty()) {
                $data->save();
                $request->session()->flash('flash_success');
                return Response::json(['success' => '1']);
            } else {
                $request->session()->flash('flash_warning');
                return Response::json(['warning' => '1']);
            }

        }

        return Response::json(['errors' => $validator->errors()]);
    }

}
