<?php

namespace App\Http\Controllers;

use App;
use App\Models\Bet;
use App\Models\BetDetail;
use App\Models\Post;
use App\Models\PostSetting;
use App\Models\ResultProvince;
use App\Models\Setting;
use App\Models\Transaction;
use Carbon\Carbon;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\JsonResponse;
use Jenssegers\Agent\Agent as Agent;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return Renderable
     */
    public function index()
    {
        $user = auth()->user();

        $locale=$user->language;
        App::setLocale($locale);
        session()->put('locale', $locale);

        // user access
        if( $user->hasAnyRole(['Admin','Master']) ) {
            abort(401, 'This action is unauthorized.');
        }
        // Agent mobile
        $agent = new Agent();
        if(!$agent->isMobile()) {
            return abort(404);
        }

        $settings = Setting::all();
        $provinces = $user->location->location_type->result_province_filters->where('weekly_id',date('N'));
        $lottery_types = $user->location->lottery_types;
        $closing_time = json_decode($settings->where('key',  'bet_closing_time')->first()->value);
        $bets= $user->bets->where('shift_id',(time() < strtotime($closing_time->day) ? 1:2))->where('lottery_type_id',$lottery_types->first()->id)->whereBetween('created_at', [Carbon::today()->toDateString().' 00:00:00', Carbon::today()->toDateString().' 23:59:59']);
        $posts= $user->location->posts->where('lottery_type_id',1);

        // load page
        return view('pages.home',compact('user','posts','bets','lottery_types','provinces','closing_time'));
    }

    /** Return Bet data to show on home page *
     * @param Request $request
     * @return JsonResponse
     */
    function ajaxBet(Request $request){
        $data=['closing_time'=>$request->shift];
        $settings = Setting::all();
        $closing_time = json_decode($settings->where('key',  'bet_closing_time')->first()->value);
        $date = date("yy-m-d", strtotime($request->date));


        if((time() < strtotime($closing_time->day) and $request->shift == 1) or (time() < strtotime($closing_time->night) and $request->shift == 2) or $request->lottery ==2){
            $user = auth()->user();
            $bet = $user->bets
                ->where('shift_id',$request->shift)
                ->where('ticket',$request->ticket)
                ->where('lottery_type_id',$request->lottery)
                ->whereBetween('created_at', [$date.' 00:00:00', $date.' 23:59:59'])->first();

            $digit= $this->getDigit($request);
            $post_amount= $this->getPostAmount($user, $request, $digit);
            $bet_num= $this->getBetNumber($request,$digit);
            $province = $this->getProvinceID($request, $user); //get province id of bet;
            $sub_total= intval($request->bet_amount) * intval($post_amount) * count($bet_num) * count($province);

            $setting=$user->user_settings
                ->where('digit', $digit)
                ->when(isset($request->lottery), function ($collection) use ($request) {
                    return $collection->where("lottery_type_id",$request->lottery);
                })->first();

            $bet_list= '
            <tr data-detail-id="'.$request->bet_detail_id.'" data-row-id="'.$request->id.'">
                <td>'.$request->post.'</td>
                <td data-operator="'.$request->operator.'">'.$request->bet_number.($request->operator_text != "" ? ' '.$request->operator_text.' '.$request->bet_type : "").'</td>
                <td>'.number_format($request->bet_amount, 0, '.', ',').'</td>
                <td>'.number_format($sub_total, 0, '.', ',').'</td>'.
                (isset($request->province) ? '<td>'.$request->province.'</td>':'')
                .'<td>
                    <a href="javascript:void(0)" onclick="editEntry(this,'.$request->id.')" class="btn btn-xs btn-default"><i class="fa fa-edit"></i></a>
                    <a href="javascript:void(0)" onclick="deleteEntry(this)" class="btn btn-xs btn-default" data-button-type="delete" style="padding-left:5px;"><i class="glyphicon glyphicon-remove"></i></a>
                    <input type="hidden" value="'.$setting->discount.'">
                    <input type="hidden" value="'.$setting->digit.'">
                </td>
            </tr>';

            $data=[
                'bet_id' => (isset($bet)) ? $bet->id : 0,
                'bet_list'=>$bet_list,
                'sub_total'=> $sub_total,
                'setting' => $setting,
                'lottery' => $request->lottery
            ];
        }
        return response()->json($data);
    }
    function ajaxOnChange(Request $request){
        $bet_list= array();
        $sub_total = array();
        $setting = array();
        $bet_id=0; $time=''; $ticket=null;
        $user = auth()->user();
        $date = date("yy-m-d", strtotime($request->date));

        $bets = $user->bets
            ->when(isset($request->lottery), function ($collection) use ($request) {
                return $collection->where("lottery_type_id",$request->lottery);
            })
            ->where('shift_id',$request->shift)
            ->whereBetween('created_at', [$date.' 00:00:00', $date.' 23:59:59']);

        if($bets->count() < $request->ticket){
            if(Carbon::today()->toDateString()==$date){
                $ticket=$bets->count()+1;
            }
            else{
                $bet = $bets->where('ticket',$bets->count())->first();
                $ticket=$bets->count();
            }
        }
        else{
            $bet = $bets->where('ticket',$request->ticket)->first();
        }

        if(isset($bet)){
            $bet_id= $bet->id;
            $time= $bet->created_at->diffInMinutes(Carbon::now());
            foreach ($bet->bet_details as $id => $bet_data) {
                $bet_numbers = json_decode($bet_data->bet_number);
                $sub_total[] = intval($bet_data->amount) * intval($bet_data->bet_multiply);
                $setting[] = ['digit'=> strlen($bet_numbers[0]), 'discount' => $bet_data->discount];
                $bet_number= implode(array_unique(str_split(implode($bet_numbers))));
                // Check Location if have more than 1 province
                $loc_provinces = $user->location->location_type->result_province_filters->where('weekly_id',date('N'))->count() > 1 ? true:false;
                $provinces = $loc_provinces? ResultProvince::whereIn('id',json_decode($bet_data->province))->get():null;

                $posts='';
                switch ($bet_data->type){
                    case 'box':
                        $bet_number .= ' x '.strlen($bet_numbers[0]);
                        break;
                    case 'default':
                        $bet_number= $bet_numbers[0];
                        break;
                    default:
                        if($bet_data->type == 'roll-first') $operator = '↑';
                        else if($bet_data->type == 'roll-middle') $operator = '←';
                        else $operator = '↓';

                        $bet_number = $bet_numbers[0].' '.$operator.' '.$bet_numbers[count($bet_numbers)-1];
                        break;
                }
                foreach ($bet_data->posts as $index => $post){
                    $posts .= $post->post.''.($index < count($bet_data->posts)-1 ? ',': '');
                }
                $bet_list[] =
                    '<tr data-detail-id="'.$bet_data->id.'" data-row-id="'.($id+1).'">
                                <td>' . $posts . '</td>
                                <td data-operator="'.$bet_data->type.'">' . $bet_number . '</td>
                                <td>' . number_format($bet_data->amount, 0, '.', ',') . '</td>
                                <td>' . number_format($sub_total[$id], 0, '.', ',') . '</td>'.
                                (isset($provinces) ? '<td>'.(count($provinces) >1 ? "Both":$provinces->first()->province).'</td>':'')
                                .'<td>
                                    <a href="javascript:void(0)" onclick="editEntry(this,'.$id.')" class="btn btn-xs btn-default"><i class="fa fa-edit"></i></a>
                                    <a href="javascript:void(0)" onclick="deleteEntry(this)" class="btn btn-xs btn-default" data-button-type="delete" style="padding-left:5px;"><i class="glyphicon glyphicon-remove"></i></a>
                                    <input type="hidden" value="'.$setting[$id]["discount"].'">
                                    <input type="hidden" value="'.$setting[$id]["digit"].'">
                                </td>
                            </tr>';
            }
        }

        $data=[
            'bet_id' => $bet_id,
            'bet_list'=>$bet_list,
            'sub_total'=> $sub_total,
            'setting' => $setting,
            'ticket' => $ticket,
            'over_time' => $time > 10 ? true:false,
            'success' => count($bet_list) > 0 ? true: false
        ];

        return response()->json($data);
    }
    function ajaxTicket(Request $request){
        $user = auth()->user();
        $date = date("yy-m-d", strtotime($request->date));
        $bets=
            $user->bets
                ->whereBetween('created_at', [$date.' 00:00:00', $date.' 23:59:59'])
                ->where('shift_id',$request->shift)
                ->where('lottery_type_id',$request->lottery);

        return response()->json($bets->count());
    }
    function ajaxPost(Request $request){
        $user = auth()->user();
        $posts= $user->location->posts->where('lottery_type_id',$request->lottery);
        return view('pages.ajax_post', compact('posts'));
    }
    /** Insert all Bet data to Bet, Bet Detail and Bet Detail Post table. *
     * @param Request $request
     * @return JsonResponse
     */
    function store(Request $request){
        //date_default_timezone_set(env('APP_TIMEZONE'));
        $settings = Setting::all();
        $closing_time = json_decode($settings->where('key',  'bet_closing_time')->first()->value);
        $data=['closing_time'=>$request->shift];

        if((time() < strtotime($closing_time->day) and $request->shift == 1) or (time() < strtotime($closing_time->night) and $request->shift == 2) or $request->lottery) {
            $updated_balance = 0;
            $user = auth()->user();
            $last_data = $user->bets->where('id', $request->bet_id)->first();

            $updated = false;
            if (isset($last_data)) {
                $updated_balance = intval($user->account->balance) + intval($last_data->grand_total) - intval($request->grand_total);
                /* Message when updated Success */
                $msg = trans('words.messages.updated_success');
                $updated = true;
            } else {
                $updated_balance = intval($user->account->balance) - intval($request->grand_total);
                /* Message when add new Success */
                $msg = trans('words.messages.bet_success');
            }

            if ($updated_balance > 0) {

                /* Insert Data to Bet table */
                $bet = Bet::updateOrCreate([
                    'id' => $request->bet_id,
                    'user_id' => $user->id,
                    'lottery_type_id' => $request->lottery ? $request->lottery:1,
                    'ticket' => $request->ticket
                ],[
                    'shift_id' => $request->shift,
                    'total' => $request->total,
                    'grand_total' => $request->grand_total,
                    'currency_id' => $user->location->currency->id
                ]);

                $transaction = Transaction::create(array(
                    'account_id' => $user->account->id,
                    'amount' => isset($last_data) ? intval($request->grand_total) - intval($last_data->grand_total):intval($request->grand_total),
                    'action' => 3, // id for Bet Action
                    'last_balance' => $user->account->balance,
                    'noted' => (isset($last_data) ? '<b>Updated</b> ':'').'Ticket number <b>'.$bet->ticket.'</b>, Shift <b>'.$bet->shift->shift.'</b>',
                    'created_at' => now(),
                    'updated_at' => now(),
                ));
                $bet->transactions()->sync($transaction->id);

                /* Insert data to Bet Detail table */
                foreach (json_decode($request->bet_data) as $bet_data) {
                    $province_id = $this->getProvinceID($request,$user, $bet_data);

                    $bet_num = $this->getBetNumber($bet_data, $this->getDigit($bet_data));
                    $post_id = $this->getPostID($user, $bet_data->post);
                    $bet_multiply = intval($this->getPostAmount($user, $bet_data, $this->getDigit($bet_data), $request->shift)) * count($bet_num) * count($province_id);

                    $bet_detail = BetDetail::updateOrCreate([
                        'id' => $bet_data->bet_detail_id,
                        'bet_id' => $bet->id
                    ], [
                        'bet_number' => json_encode($bet_num),
                        'amount' => $bet_data->bet_amount,
                        'type' => $bet_data->operator,
                        'discount' => $bet_data->discount,
                        'bet_multiply' => $bet_multiply,
                        'province' => $request->lottery && $request->lottery ==2 ? '[0]':$province_id,
                    ]);
                    $bet_detail->posts()->sync($post_id);
                }
                $delete_id = explode(',', $request->delete_id);
                if ($delete_id[0] != 0) {
                    BetDetail::destroy($delete_id);
                }
                $data = [
                    'ticket' => $bet->ticket,
                    'updated_balance' => $updated_balance,
                    'success' => true,
                    'updated' => $updated,
                    'msg' => $msg
                ];

                /* Update Account Balance */
                $user->account->update(['balance' => $updated_balance]);

            } else {
                $data = ['msg' => 'ទឹកប្រាក់របស់លោកអ្នកមិនគ្រប់គ្រាន់សម្រាប់ការចាក់នេះទេ!'];
            }
        }
        return response()->json($data);
    }

    function getProvinceID($request,$user, $data=null){
        $province_id=new Collection();
        $data = isset($data) ? $data:$request;
        if($request->shift == 1){
            $provinces= $user
                ->location->location_type->result_province_filters->where('weekly_id',date('N'))
                ->when(isset($data->province) && $data->province != 'Both', function ($collection) use ($data) {
                    return $collection->where("result_province.province",$data->province);
                });
            foreach ($provinces as $province)
                $province_id->push($province->result_province_id);
        }
        else{
            $province_id = collect([22]);
        }
        return $province_id;
    }
    /** Return Digit of Bet number*
     * @param $request
     * @return int
     */
    function getDigit($request){
        if($request->operator == 'box')
            return intval($request->bet_type);
        else
            return strlen($request->bet_number);

    }
    /** Return Bet Number *
     * @param Request $request
     * @param $digit
     * @return array
     */
    function getBetNumber($request,$digit){
        switch ($request->operator){
            case 'roll-first':
                $bet_num = array_map('strval',range($request->bet_number, $request->bet_type,str_pad(1, $digit, 0)));
                break;
            case 'roll-middle':
                $bet_num = array_map('strval',range($request->bet_number, $request->bet_type,str_pad(1, $digit-1, 0)));
                break;
            case 'roll-last':
                $bet_num = array_map('strval',range($request->bet_number, $request->bet_type));
                break;
            case 'box':
                $bet_num = $this->permute($request->bet_number,(strlen($request->bet_number)-$digit)+1);
                break;
            default:
                $bet_num=[$request->bet_number];
                break;
        }
        foreach ($bet_num as $key=>$bet){
            if(strlen($request->bet_type) > 1 and strlen($bet) < strlen($request->bet_number)){
                $bet_num[$key] = str_pad($bet, strlen($request->bet_number), '0', STR_PAD_LEFT);
            }
        }
        return $bet_num;
    }
    /** Return collection Post ID by collection Post *
     * @param $user
     * @param $post_name
     * @return Collection
     */
    function getPostID($user, $post_name){
        /** Collect Post from Post Table by selected post on bet form **/
        $posts= $user->location->posts->whereIn('post', explode(",", $post_name));

        /** Collect Post ID for each Betting **/
        $post_id=new Collection();
        foreach ($posts as $post)
            $post_id->push($post->id);
        return $post_id;
    }
    /** Return Post Amount *
     * @param $user
     * @param $request
     * @param $digit
     * @param string $shift
     * @return int
     */
    function getPostAmount($user, $request, $digit, $shift = '' ){
        $post_amount=0;
        $post_id= $this->getPostID($user,$request->post);

        /** Get post setting by shift and post **/
        $post_settings= PostSetting::where('shift_id', $shift =='' ? $request->shift : $shift)
            ->whereIn('post_id', $post_id)->get();

        foreach ($post_settings as $settings){
            /** Get number of Post Amount **/
            foreach (json_decode($settings->setting) as $setting){
                if($setting->location_type_id == $user->location->location_type->id){
                    $post_amount += $setting->value->{$digit . 'D'};
                }
            }
        }

        return $post_amount;
    }
    /** Permutation number by n digit *
     * @param $arg
     * @param $n
     * @return array
     */
    function permute($arg,$n) {
        $array = is_string($arg) ? str_split($arg) : $arg;
        if(count($array) === $n)
            return $array;
        $result = array();
        foreach($array as $key => $item)
            foreach($this->permute(array_diff_key($array, array($key => $item)),$n) as $p)
                $result[] = $item . $p;
        return array_values(array_unique($result));
    }

    function getPostCollection($user,$lottery_type_id){
        $posts= Post::whereHas('post_settings', function($item) use ($lottery_type_id) {
            $item->where('lottery_type_id', $lottery_type_id);
        })
            ->whereHas('location_types', function ($item) use ($user) {
                $item->where('id', $user->location->location_type->id);
            })
            ->get();
        return $posts;
    }
}
