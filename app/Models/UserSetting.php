<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class UserSetting extends Model
{
    public function user(){
        return $this->belongsTo(User::class);
    }

    public function lottery_type(){
        return $this->belongsTo(LotteryType::class);
    }
}
