<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Weekly extends Model
{
    public function result_province_filters()
    {
        return $this->hasMany(ResultProvinceFilter::class);
    }
}
