<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LocationType extends Model
{
    public function locations(){
        return $this->hasMany(Location::class);
    }

    public function result_filters(){
        return $this->hasMany(ResultFilter::class);
    }

    public function result_province_filters(){
        return $this->hasMany(ResultProvinceFilter::class);
    }
/*
    public function posts(){
        return $this->belongsToMany(Post::class);
    }*/

    public function post_settings(){
        return $this->hasMany(PostSetting::class);
    }

}
