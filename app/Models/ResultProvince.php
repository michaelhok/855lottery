<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ResultProvince extends Model
{
    public function results()
    {
        return $this->hasMany(Result::class);
    }

    public function result_filters()
    {
        return $this->hasMany(ResultFilter::class);
    }

    public function result_province_filters()
    {
        return $this->hasMany(ResultProvinceFilter::class);
    }
}
