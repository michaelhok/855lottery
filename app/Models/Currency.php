<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Currency extends Model
{
    public function locations(){
        return $this->hasMany(Location::class);
    }

    public function bets(){
        return $this->hasMany(Bet::class);
    }
}
