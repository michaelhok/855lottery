<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
    public function location_type(){
        return $this->belongsTo(LocationType::class);
    }

    public function users(){
        return $this->hasMany(User::class);
    }

    public function lottery_types(){
        return $this->belongsToMany(LotteryType::class);
    }

    public function posts(){
        return $this->belongsToMany(Post::class);
    }

    public function currency(){
        return $this->belongsTo(Currency::class);
    }
}
