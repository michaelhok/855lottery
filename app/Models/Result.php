<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Result extends Model
{
    protected $fillable = ['result_province_id', 'shift_id', 'lottery_type_id', 'result'];

    public function result_province(){
        return $this->belongsTo(ResultProvince::class);
    }

    public function shift(){
        return $this->belongsTo(Shift::class);
    }

    public function lottery_type(){
        return $this->belongsTo(LotteryType::class);
    }

}
