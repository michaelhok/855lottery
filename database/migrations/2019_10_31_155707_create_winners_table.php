<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWinnersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('winners', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('win_number');
            $table->integer('bet_amount');
            $table->integer('win_amount');
            $table->integer('post_id');
            $table->integer('shift_id');
            $table->integer('bet_id');
            $table->integer('user_id');
            $table->integer('payee')->default(0);
            $table->enum('status', ['unpaid', 'paid','expired'])->default('unpaid');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('winners');
    }
}
