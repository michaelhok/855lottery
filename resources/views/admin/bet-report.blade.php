@extends('layouts.admin-master')

@section('title', $view_name)

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Bet Report
            {{--<small>advanced tables</small>--}}
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('dashboard') }}"><i class="fas fa-tachometer-alt"></i> Home</a></li>
            <li><a href="{{ route('bet-report') }}">Bet Report</a></li>
            <li class="active">List</li>
        </ol>
    </section>

    <div class="box-header">
        <div class="hidden-print with-border">
           {{-- <a href="#" class="btn btn-primary ladda-button" data-toggle="modal" data-target="#myModal" data-style="zoom-in">
            <span class="ladda-label"><i class="fa fa-plus"></i> Add Role</span></a>--}}
            <button class="btn btn-default border-radius" onclick="reload_page()"><i class="fas fa-redo"></i> Reload</button>
        </div>
    </div>
    <!-- /.box-header -->

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">

                        <!-- Filter Shift and Post Category -->
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="row">
                            <div class="col-sm-2">
                                <div class="form-group">
                                    <button type="button" class="btn btn-default btn-block" id="date-range">
                                        <i class="fa fa-calendar"></i>&nbsp;
                                        <span></span>
                                        &nbsp;<i class="fa fa-caret-down"></i>
                                    </button>
                                </div>
                            </div>
                            @if(isset($shifts))
                                <div class="col-sm-2">
                                    <select class="form-control" id="shift">
                                        @foreach($shifts as $shift)
                                            <option value="{{$shift->id}}">{{$shift->shift}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            @endif
                            @if(isset($locations))
                                <div class="col-sm-2">
                                    <select class="form-control" id="location">
                                        <option value="default" selected="selected">--- Select Location ---</option>
                                        @foreach($locations as $location)
                                            <option value="{{$location->id}}">{{$location->location}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            @endif
                            @if(isset($users))
                                <div class="col-sm-2">
                                    <select class="form-control" id="user">
                                        <option value="default" selected="selected">--- Select Agent ---</option>
                                        @foreach($users as $user)
                                            <option value="{{$user->id}}">{{$user->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            @endif
                        </div>
                        <table id="table" class="table table-bordered table-hover dt-responsive">
                            <thead>
                            <tr>
                                <th>No</th>
                                <th>User</th>
                                <th>2 Digit</th>
                                <th>3 Digit</th>
                                <th>4 Digit</th>
                                {{--<th>Sub Total</th>--}}
                            </tr>
                            </thead>

                            <tfoot>
                            <tr>
                                <th>No</th>
                                <th>User</th>
                                <th>2 Digit</th>
                                <th>3 Digit</th>
                                <th>4 Digit</th>
                                {{--<th>Sub Total</th>--}}
                            </tr>
                            </tfoot>
                        </table>
                        {{--<div class="col-sm-4 pull-right">
                            <p class="lead">Grand Total</p>

                            <div class="table-responsive">
                                <table class="table">
                                    <tr id="total_bet">
                                        <th>Grand Total Bet:</th>
                                        <td>0</td>
                                    </tr>
                                    <tr id="total_lose">
                                        <th>Grand Total Lose:</th>
                                        <td>0</td>
                                    </tr>
                                    <tr id="total">
                                        <th>Grand Total:</th>
                                        <td>0</td>
                                    </tr>
                                </table>
                            </div>
                        </div>--}}
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
        </div>
    </section>

@stop

@section('script')
    <script type="text/javascript">
        /* Custom filtering function which will search data in column four between two values */

        $(document).ready(function(){

            $('.select2').select2();
            //Date range as a button
            $('#date-range span').html(moment().format('MMM D, YYYY') + ' - ' + moment().format('MMM D, YYYY'));
            $('#date-range').daterangepicker(
                {
                    autoApply: true,
                    ranges   : {
                        'Today'       : [moment(), moment()],
                        'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                        'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
                        'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                        'This Month'  : [moment().startOf('month'), moment().endOf('month')],
                        'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                    },
                    startDate: moment(),
                    endDate  : moment(),
                    maxDate: moment(),
                    //alwaysShowCalendars: true,
                    showCustomRangeLabel: true,
                    opens: "right"
                },
                function (start, end) {
                    $('#date-range span').html(start.format('MMM D, YYYY') + ' - ' + end.format('MMM D, YYYY'))
                }
            );

            let table = fetch_data();

            $('#date-range, #shift, #location, #user').on('change apply.daterangepicker', function(){

                if($(this).attr('id')=='location'){
                    $.ajax({
                        type: 'GET',
                        url: '{{ route('ajax-user') }}',
                        data: {location: $('#location').val(),parent: true},
                        success: function (data) {
                            const option = new Option('--- Select Agent ---', 'default', true, true);
                            $('#user').html(option);
                            $.each(data, function (index) {
                                const newOption = new Option(data[index].agent_name+' ( '+data[index].agent_role+' )', data[index].agent_id, false, false);
                                $('#user').append(newOption);
                            });
                        }
                    });
                }

                $('#table').DataTable().destroy();
                table = fetch_data();
            });

            function fetch_data() {
                // Data table for serverside
                const table = $('#table').DataTable({
                    processing: true,
                    serverSide: true,
                    ajax:{
                        url: "{{ route('bet-report.show') }}",
                        dataType: "json",
                        type: "GET",
                        data:{
                            _token: "{{ csrf_token() }}",
                            route:'bet-report',
                            date_range: $('#date-range span').html(),
                            shift: $('#shift').val(),
                            user: $('#user').val(),
                            location: $('#location').val(),
                        }
                    },
                    columns: [
                        { data: "no" },
                        { data: "user" },
                        { data: "2digit" },
                        { data: "3digit" },
                        { data: "4digit" },
                        /*{ data: "sub_total" },*/

                    ],
                    columnDefs: [
                        {searchable: false, orderable: false, targets: 0},
                        {className: 'all', targets: [1,2] }
                    ],
                    order: [[ 1, 'asc' ]],
                    pageLength: 100,

                    /*drawCallback : function ( row, data, start, end, display ) {
                        var api = this.api(), data;

                        // converting to interger to find total
                        const intVal = function (i) {
                            return typeof i === 'string' ?
                                i.replace(/[\$,]/g, '') * 1 :
                                typeof i === 'number' ?
                                    i : 0;
                        };

                        const totalBet = api
                            .column(2)
                            .data()
                            .reduce(function (a, b) {
                                return intVal(a) + intVal(b);
                            }, 0);

                        const totalLose = api
                            .column(3)
                            .data()
                            .reduce(function (a, b) {
                                return intVal(a) + intVal(b);
                            }, 0);

                        const Total = api
                            .column(4)
                            .data()
                            .reduce(function (a, b) {
                                return intVal(a) + intVal(b);
                            }, 0);


                        // Update footer by showing the total with the reference of the column index
                        $('#total_bet td').html(Intl.NumberFormat('en-US').format(totalBet));
                        $('#total_lose td').html(Intl.NumberFormat('en-US').format(totalLose));
                        $('#total td').html(Intl.NumberFormat('en-US').format(Total));
                        //$('#total span').html(Intl.NumberFormat('en-US', { style: 'currency', currency: 'USD' }).format(Total));
                    },*/
                });

                return table;
            }
        });
    </script>
@endsection
