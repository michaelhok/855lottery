@if(count($user_settings) > 0)
    @foreach($user_settings->unique("lottery_type_id") as $index=>$lottery_type)
        <div id="{{$lottery_type->lottery_type->type}}" class="tab-pane fade {{$index==0 ?'in active':''}}">
            <div class="row">
                @foreach($user_settings->where("lottery_type_id",$lottery_type->lottery_type_id) as $i=>$user_setting)
                        <div class="form-group">
                            <label class="col-xs-2 control-label">{{$user_setting->digit}}</label>
                            <div class="col-xs-3">
                                <label>Discount</label>
                                <input type="number" name="discount[{{$i}}]" class="form-control input-discount" placeholder="Discount" value="{{$user_setting->discount}}" required>
                            </div>
                            <div class="col-xs-3">
                                @if(gettype(json_decode($user_setting->bet_win)) == 'object')
                                    @foreach(json_decode($user_setting->bet_win) as $key=>$bet_win)
                                        <label>{{$user_setting->lottery_type->posts->where('id',$key)->first()->post}}</label>
                                        <input type="number" name="bet_win[{{$i}}][{{$key}}]" class="form-control input-betWin" placeholder="{{$user_setting->lottery_type->posts->where('id',$key)->first()->post}}" value="{{$bet_win}}" required>
                                    @endforeach
                                @else
                                    <label>Bet Win</label>
                                    <input type="number" name="bet_win[{{$i}}]" class="form-control input-betWin" placeholder="Bet Win" value="{{$user_setting->bet_win}}" required>
                                @endif
                            </div>
                            <div class="col-xs-3">
                                <label>Bet Limit</label>
                                <input type="number" name="bet_limit[{{$i}}]" class="form-control input-betLimit" placeholder="Bet Limit" value="{{$user_setting->bet_limit}}" required>
                            </div>
                            <input type="hidden" name="id[{{$i}}]" class="form-control" value="{{$user_setting->id}}">
                        </div>
                @endforeach
            </div>
        </div>
    @endforeach
    {{--<div id="night" class="tab-pane fade">
        <div class="row">
            @foreach($user_settings as $user_setting)
                @if($user_setting->lottery_type_id==2)
                    <div class="form-group">
                        <label class="col-xs-2 control-label">{{$user_setting->digit}}</label>
                        <div class="col-xs-3">
                            <label>Discount</label>
                            <input type="number" name="discount[]" class="form-control input-discount" placeholder="Discount" value="{{$user_setting->discount}}" required>
                        </div>
                        <div class="col-xs-3">
                            <label>Bet Win</label>
                            <input type="number" name="bet_win[]" class="form-control input-betWin" placeholder="Bet Win" value="{{$user_setting->bet_win}}" required>
                        </div>
                        <div class="col-xs-3">
                            <label>Bet Limit</label>
                            <input type="number" name="bet_limit[]" class="form-control input-betLimit" placeholder="Bet Limit" value="{{$user_setting->bet_limit}}" required>
                        </div>
                        <input type="hidden" name="id[]" class="form-control" value="{{$user_setting->id}}">
                    </div>
                @endif
            @endforeach
        </div>
    </div>--}}
@endif

<script type="text/javascript">
    $(document).ready(function () {
        // check update form
        $('form#myForm-setting').each(function() {
            $(this).data('serialized', $(this).serialize());
        }).on('change input', function() {
            $(this).find('input:submit, button:submit').attr('disabled', $(this).serialize() == $(this).data('serialized'));
        }).find('input:submit, button:submit').attr('disabled', true);
    });
</script>
